package SilverScreen.util;

import javafx.scene.control.Alert;
import javafx.stage.Window;

public class AlertUtils {

    public static final String ALERT_TITLE = "No selection";
    public static final String ALERT_HEADER_TEXT = "No row selected";
    public static final String ALERT_MESSAGE = "Please select a row in the table.";
    public static final String BIRTHDAY_ALERT_TITLE = "Birthday notice";
    public static final String BIRTHDAY_ALERT_MESSAGE = "Today is this person's birthday";

    public static void showAlert(Alert.AlertType alertType, Window owner, String title, String headerText, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.setContentText(message);
        alert.initOwner(owner);
        alert.show();
    }
}
