package SilverScreen.util;

import SilverScreen.model.Human;
import javafx.scene.control.TreeItem;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class DataUtils {

    public static List<TreeItem<Human>> getData() {
        List<TreeItem<Human>> items = new ArrayList<>();
        items.add(new TreeItem<>(new Human("Ben Kowalewicz", 32, LocalDate.of(1980, 8, 25))));
        items.add(new TreeItem<>(new Human("Ian Michael D'Sa", 35, LocalDate.of(1977, 3, 11))));
        items.add(new TreeItem<>(new Human("Jonathan Gallant", 37, LocalDate.of(1975, 4, 19))));
        items.add(new TreeItem<>(new Human("Aaron Solowoniuk", 33, LocalDate.of(1979, 11, 26))));
        return items;
    }
}
