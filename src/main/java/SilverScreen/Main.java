package SilverScreen;

import SilverScreen.controller.HumanOverviewController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class Main extends Application {

    private Stage primaryStage;

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/main.fxml"));
        AnchorPane rootLayout = loader.load();
        primaryStage.setTitle("Silver Screen");
        primaryStage.setScene(new Scene(rootLayout, 445, 417));
        primaryStage.show();

        this.primaryStage = primaryStage;

        HumanOverviewController controller = loader.getController();
        controller.setMainApp(this);
    }

    public static void main(String[] args) {
        launch(args);
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }
}
