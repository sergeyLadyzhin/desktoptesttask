package SilverScreen.controller;

import static SilverScreen.util.AlertUtils.*;

import java.io.IOException;
import java.time.LocalDate;
import java.time.MonthDay;

import SilverScreen.Main;
import SilverScreen.model.Human;
import SilverScreen.util.AlertUtils;
import SilverScreen.util.DataUtils;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

public class HumanOverviewController {
    @FXML
    private TreeTableView<Human> treeTableView;

    @FXML
    private TreeTableColumn<Human, String> nameCol;

    @FXML
    private TreeTableColumn<Human, Number> ageCol;

    @FXML
    private TreeTableColumn<Human, LocalDate> birthdayCol;

    private Main main;
    private TreeItem<Human> root = new TreeItem<>(new Human());

    @FXML
    void initialize() {
        configureTreeTableView();
    }

    private void configureTreeTableView() {
        nameCol.setCellValueFactory(param -> param.getValue().getValue().nameProperty());
        ageCol.setCellValueFactory(param -> param.getValue().getValue().ageProperty());
        birthdayCol.setCellValueFactory(param -> param.getValue().getValue().birthdayProperty());

        treeTableView.setRoot(root);
        root.getChildren().setAll(DataUtils.getData());
        treeTableView.setShowRoot(false);

        treeTableView.setRowFactory( tv -> {
            TreeTableRow<Human> row = new TreeTableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && !row.isEmpty()) {
                    MonthDay birthday = MonthDay.of(row.getItem().getBirthday().getMonth(),
                            row.getItem().getBirthday().getDayOfMonth());
                    MonthDay currentMonthDay = MonthDay.from(LocalDate.now());
                    if (currentMonthDay.equals(birthday)) {
                        AlertUtils.showAlert(Alert.AlertType.INFORMATION, main.getPrimaryStage(), BIRTHDAY_ALERT_TITLE, "", BIRTHDAY_ALERT_MESSAGE);
                    }
                }
            });
            return row ;
        });
    }

    @FXML
    private void handleDeleteHuman() {
        int selectedIndex = treeTableView.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            treeTableView.getRoot().getChildren().remove(selectedIndex);
        } else {
            AlertUtils.showAlert(Alert.AlertType.WARNING, main.getPrimaryStage(), ALERT_TITLE, ALERT_HEADER_TEXT, ALERT_MESSAGE);
        }
    }

    private boolean showHumanEditDialog(Human human) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/fxml/humanEditForm.fxml"));
            loader.load();

            Parent root = loader.getRoot();
            Stage stage = new Stage();
            stage.setTitle("Add/Edit Human Dialog");
            stage.setScene(new Scene(root));

            HumanEditController controller = loader.getController();
            controller.setDialogStage(stage);
            controller.setPerson(human);

            stage.showAndWait();

            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @FXML
    private void handleNewPerson() {
        Human human = new Human();
        boolean okClicked = showHumanEditDialog(human);
        if (okClicked) {
            root.getChildren().add(new TreeItem<>(human));
        }
    }

    @FXML
    private void handleEditPerson() {
        TreeItem<Human> selectedPerson = treeTableView.getSelectionModel().getSelectedItem();
        if (selectedPerson != null) {
            showHumanEditDialog(selectedPerson.getValue());
        } else {
            AlertUtils.showAlert(Alert.AlertType.WARNING, main.getPrimaryStage(), ALERT_TITLE, ALERT_HEADER_TEXT, ALERT_MESSAGE);
        }
    }

    public void setMainApp(Main main) {
        this.main = main;
    }
}

