package SilverScreen.controller;

import java.time.LocalDate;

import SilverScreen.model.Human;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class HumanEditController {
    @FXML
    private TextField nameField;

    @FXML
    private TextField ageField;

    @FXML
    private TextField birthdayField;

    private Stage stage;
    private Human human;
    private boolean okClicked = false;

    @FXML
    void initialize() {
    }

    void setDialogStage(Stage stage) {
        this.stage = stage;
    }

    void setPerson(Human human) {
        this.human = human;

        if (human.nameProperty() != null) {
            nameField.setText(human.getName());
        }
        if (human.ageProperty().greaterThan(0).getValue()) {
            ageField.setText(Integer.toString(human.getAge()));
        }
        if (human.birthdayProperty().isNotNull().getValue()) {
            birthdayField.setText(human.getBirthday().toString());
        }
    }

    @FXML
    private void handleOk() {
        human.setName(nameField.getText());
        human.setAge(Integer.parseInt(ageField.getText()));
        human.setBirthday(LocalDate.parse(birthdayField.getText()));

        okClicked = true;
        stage.close();
    }

    @FXML
    private void handleCancel() {
        stage.close();
    }

    boolean isOkClicked() {
        return okClicked;
    }
}
